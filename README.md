# Managed Container Platform - Software Delivery Workshop

This lab demostrates Kubernetes CI/CD with Google Cloud devops tools [Google Cloud Deploy](https://cloud.google.com/deploy), [Cloud Build](https://cloud.google.com/build), and [Artifact Registry](https://cloud.google.com/artifact-registry), and [Cloud Source Repositories](https://cloud.google.com/source-repositories). The example app is based on a simple Python Flask example app named "Population Stats" and uses [Kustomize](https://kustomize.io/) overlays for manifest generation.

[[_TOC_]]

## Prerequisites

Before you begin, ensure you have the following requirements met:

- This guide has only been tested in [Google Cloud Shell](https://cloud.google.com/shell/docs/features).
- This guide assumes you have `owner` IAM permissions on the Google project. In production, you do not require `owner` permission. Please check [ASM documentation](https://cloud.google.com/service-mesh/v1.10/docs/scripted-install/gke-project-cluster-setup#setting_up_your_project) for exact IAM requirements.

## Objectives

- **Lab Setup**

  - Run terraform to set up the environment needed for the lab.
  - Analyze your environment and verify all required resources are there.

- **Lab Steps**
  - Creating and running an application delivery pipeline using [Cloud Build](https://cloud.google.com/build) and [Cloud Deploy](https://cloud.google.com/deploy).
  - Safely promoting an application to production through a multi-stage pipeline (using both GCP Console UI and CLI).
  - Developing, testing and deploying changes to your application.
  - Releasing changes to production.
  - Rolling back changes.

## Lab Setup

The following steps are required for demo setup.

### Setting up your environment

To set up your environment, follow these steps.

1.  In the Google Cloud Console, activate Cloud Shell.

    [ACTIVATE CLOUD SHELL](https://console.cloud.google.com/?cloudshell=true&_ga=2.89525019.1226080234.1626725693-1104188948.1619545283)

At the bottom of the Cloud Console page, a [Cloud Shell](https://cloud.google.com/shell/docs/features) session starts and displays a command-line prompt. Cloud Shell is a shell environment with the Cloud SDK and the [gcloud command-line tool](https://cloud.google.com/sdk/gcloud) already installed, and with values already set for your current project. It can take a few seconds for the session to initialize.

1.  Create a `WORKDIR` to store all associated files for this tutorial. You can then delete the folder when you're done.

    ```bash
    mkdir -p mcp-software-delivery && cd mcp-software-delivery && export WORKDIR=$(pwd)
    ```

1.  Define your PROJECT_ID.

    ```bash
    export PROJECT_ID=YOUR PROJECT ID
    ```

1.  Clone this repo.

    ```bash
    # Initialize the Git client if needed
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"

    git clone https://gitlab.com/demos777/mcp-cicd.git ${WORKDIR}/mcp-cicd
    cd ${WORKDIR}/mcp-cicd
    ```

1.  Create a GCS bucket for to store terraform state.

    ```bash
    gcloud config set project ${PROJECT_ID}
    gsutil mb -p ${PROJECT_ID} gs://${PROJECT_ID}
    gsutil versioning set on gs://${PROJECT_ID}
    ```

1.  Prepare backend and variables for terraform.

    ```bash
    cd ${WORKDIR}/mcp-cicd/bootstrap
    envsubst < backend.tf_tmpl > backend.tf
    envsubst < variables.tfvars_tmpl > variables.tfvars
    ```

1.  Initialize and apply terraform.

    ```bash
    terraform init -var-file=variables.tfvars
    terraform plan -var-file=variables.tfvars
    terraform apply -auto-approve -var-file=variables.tfvars
    ```

    The output is similar to the following:

    ```
    Apply complete! Resources: 18 added, 0 changed, 0 destroyed.
    ```

    > This step can take 15 - 20 minutes to complete.

    Terraform creates the following resources in your project:

    - 4 [GKE autopilot](https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-overview) clusters. One each for dev, test, stage and production environments.
    - A [Cloud Source repository](https://cloud.google.com/source-repositories) for application source code, Kubernetes manifests, skaffold, and CICD pipelines.
    - A Cloud Source repository trigger to trigger a Cloud Build pipeline when a commit is made to a branch.
    - An [Artifact registry](https://cloud.google.com/artifact-registry) for container images.
    - The required IAM roles for Cloud Build service account to be able to build and deploy [Cloud Deploy](https://cloud.google.com/deploy) pipelines.

### Analyzing your environment

You can now ensure that all resources are created successfully in your project.

1.  Verify that the Cloud Source Respoistory was successfully created.

    ```bash
    gcloud --project=${PROJECT_ID} source repos list
    ```

    The output is similar to the following:

    ```
    REPO_NAME: pop-stats-repo
    ...
    ```

1.  Verify that Artifact registry was successfully created.

    ```bash
    gcloud --project=${PROJECT_ID} artifacts repositories list
    ```

    The output is similar to the following:

    ```
    REPOSITORY: pop-stats
    FORMAT: DOCKER
    ...
    ```

1.  Verify all clusters are running.

    ```bash
    gcloud beta container clusters list --format='table(name,status)'
    ```

    The output is similar to the following:

    ```
    NAME: gke-dev
    STATUS: RUNNING

    NAME: gke-prod
    STATUS: RUNNING

    NAME: gke-stage
    STATUS: RUNNING

    NAME: gke-test
    STATUS: RUNNING
    ```

1.  Get cluster credentials and rename contexts for easy switching.

    ```bash
    touch ${WORKDIR}/kubeconfig && export KUBECONFIG=${WORKDIR}/kubeconfig
    gcloud container clusters get-credentials gke-dev --zone us-central1
    gcloud container clusters get-credentials gke-test --zone us-central1
    gcloud container clusters get-credentials gke-stage --zone us-central1
    gcloud container clusters get-credentials gke-prod --zone us-central1

    # Rename cluster contexts
    kubectl config rename-context gke_${PROJECT_ID}_us-central1_gke-dev gke-dev
    kubectl config rename-context gke_${PROJECT_ID}_us-central1_gke-test gke-test
    kubectl config rename-context gke_${PROJECT_ID}_us-central1_gke-stage gke-stage
    kubectl config rename-context gke_${PROJECT_ID}_us-central1_gke-prod gke-prod
    ```

## Lab Steps

### Creating and running an application delivery pipeline

You can now create a Cloud Deploy pipeline which allows you to gracefully release your applications into production.

1.  Create a Cloud Deploy pipeline.

    ```bash
    sed -e "s/GCP_PROJECT_ID/${PROJECT_ID}/" ${WORKDIR}/mcp-cicd/clouddeploy.yaml_tmpl > ${WORKDIR}/mcp-cicd/clouddeploy.yaml
    gcloud --project=${PROJECT_ID} deploy apply --file ${WORKDIR}/mcp-cicd/clouddeploy.yaml \
      --region=us-central1
    ```

    The output is similar to the following:

    ```
    Waiting for the operation on resource projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/deliveryPipelines/pop-stats-pipeline...done.
    Created Cloud Deploy resource: projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/deliveryPipelines/pop-stats-pipeline.
    Waiting for the operation on resource projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/targets/test...done.
    Created Cloud Deploy resource: projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/targets/test.
    Waiting for the operation on resource projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/targets/staging...done.
    Created Cloud Deploy resource: projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/targets/staging.
    Waiting for the operation on resource projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/targets/prod...done.
    Created Cloud Deploy resource: projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/targets/prod.
    ```

1.  Initialize the repo with the Population Statistics (or pop-stats) application source code and Kubernetes manifests.

    ```bash
    cd ${WORKDIR}
    gcloud source repos clone pop-stats-repo
    cd ${WORKDIR}/pop-stats-repo
    git checkout -b main
    cp -r ${WORKDIR}/mcp-cicd/app .
    cp -r ${WORKDIR}/mcp-cicd/k8s .
    cp -r ${WORKDIR}/mcp-cicd/cloudbuild.yaml .
    cp -r ${WORKDIR}/mcp-cicd/skaffold.yaml .
    # Commit to source repo
    git add . && git commit -am "initial commit"
    git push -u origin main
    ```

    In a few moments, this triggers a Cloud Build pipeline.

1.  Inspect the Cloud Build pipeline.

    ```bash
    gcloud builds list
    ```

    The output is similar to the following:

    ```
    ID: 6ae8296a-9226-4ff3-af9a-ad3e2644001b
    CREATE_TIME: 2022-02-09T00:48:17+00:00
    DURATION: 9S
    SOURCE: pop-stats-repo@9b054ad4aa9f1c050f64ce23dfa7229794b4ca10
    IMAGES: -
    STATUS: WORKING
    ```

1.  View the Cloud Build logs.

    ```bash
    export BUILD_ID=$(gcloud --project=${PROJECT_ID} builds list --format='value(id)')
    gcloud builds log ${BUILD_ID}
    ```

    You can also view the logs in the [Cloud Build History](https://console.cloud.google.com/cloud-build/builds) page.

1.  Wait until the Cloud Build pipeline successfully completes and then view Artifact Registry and ensure you have the `pop-stats` app container image.

    ```bash
    gcloud --project=${PROJECT_ID} artifacts docker images list us-central1-docker.pkg.dev/$PROJECT_ID/pop-stats
    ```

    The output is similar to the following:

    ```
    IMAGE: us-central1-docker.pkg.dev/qwiklabs-gcp-01-dfaea88a1ea2/pop-stats/pop-stats
    DIGEST: sha256:2612660cb79dbbd345ec39e85681512580f1893587821c59beb77951cae6b808
    ...
    ```

    You can also view this in GCP Console by navigating to [Artifact Repository](https://console.cloud.google.com/artifacts) page.

1.  Wait a few moments and view the Cloud Deploy pipeline status.

    ```bash
    gcloud --project=${PROJECT_ID} deploy releases list --delivery-pipeline=pop-stats-pipeline --region=us-central1
    ```

    The relevant output is similar to the following:

    ```
    ...
    targetRenders:
      prod:
          renderingBuild: projects/40658068861/locations/us-central1/builds/6304869a-c64a-4193-9ac6-11d0f03834d5
          renderingState: IN_PROGRESS
      staging:
          renderingBuild: projects/40658068861/locations/us-central1/builds/9fef8ea8-9eeb-4441-bd28-6c4c3d44debc
          renderingState: IN_PROGRESS
      test:
          renderingBuild: projects/40658068861/locations/us-central1/builds/ad336cf8-d9a8-4686-b767-9438cfa07824
          renderingState: IN_PROGRESS
    ...
    ```

    Wait a few moments longer if you do not see the pipeline immediately.

    You can also view Cloud Deploy pipelines by navigating to [Cloud Deploy pipelines](https://console.cloud.google.com/deploy/delivery-pipelines).

1.  Wait a few moments until the `test` stage of the pipeline is finished. You can view this in the Console or the CLI by running the following command:

    ```bash
    gcloud --project=${PROJECT_ID} deploy releases list --delivery-pipeline=pop-stats-pipeline --region=us-central1 --format='value(targetRenders.test.renderingState)'
    ```

    The output is similar to the following:

    ```
    SUCCEEDED
    ```

    The `test` stage in the pipeline deploys the application to the `gke-test` GKE cluster using the manifests in the `/k8s/overlays/test` Kustomize folder. The following resource are deployed to the `gke-test` cluster.

    - A `pop-stats` Kubernetes Service of Type LoadBalancer.
    - A `pop-stats` Kubernetes Deployment.
    - A Kubernetes Horizontal Pod Autoscaler for the `pop-stats` Deployment.

1.  Verify that the `pop-stats` application resources are present in the `gke-test` cluster.

    ```bash
    kubectl --context=gke-test -n default get service,deployment,hpa
    ```

    The output is similar to the following:

    ```
    NAME                 TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)          AGE
    service/kubernetes   ClusterIP      10.87.128.1     <none>          443/TCP          25m
    service/pop-stats    LoadBalancer   10.87.130.166   35.223.143.20   8080:31770/TCP   12m

    NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/pop-stats   1/1     1            1           12m

    NAME                                            REFERENCE              TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
    horizontalpodautoscaler.autoscaling/pop-stats   Deployment/pop-stats   0%/90%    1         2         1          12m
    ```

    You can also view this from the GCP Console in the [Kubernetes Engine](https://console.cloud.google.com/kubernetes/) page.

1.  Wait until you see an `EXTERNAL-IP` address populated in the `pop-stats` Service and the Deployment is Available. Navigate to the following address on port 8080:

    ```bash
    kubectl --context=gke-test -n default get service pop-stats -ojsonpath='{.status.loadBalancer.ingress[].ip}'
    ```

    > Ensure that you are navigating to port 8080 and not the default port 80.

    The top banner on the application should say `Population Stats Test`. The `Test` in the banner indicates that it is running on `gke-test` cluster. You can now promote this pipeline all the way to production.

### Promoting your application to production

You can either use the Web Console UI or Cloud Shell CLI to promote your pipeline. Promotion to production is a two stage process: - Promote from `test` to `stage`. You can use the Cloud Shell CLI to perform this action. - Promote from `stage` to `prod`. You can use the Web Consolte UI to perform this action.

1.  Promote the current delivery pipeline from `test` to `stage` by running the following commands:

    ```bash
    # Get the release name
    export RELEASE_NAME=$(gcloud --project=${PROJECT_ID} deploy releases list --delivery-pipeline=pop-stats-pipeline --region=us-central1 --format='value(name)')
    # Promote release to staging
    gcloud --project=${PROJECT_ID} deploy releases promote --release=${RELEASE_NAME} --delivery-pipeline=pop-stats-pipeline --region=us-central1 --to-target=staging
    ```

    The output is similar to the following:

    ```
    Promoting release rel-57c07e1 to target staging.
    Do you want to continue (Y/n)?
    Creating rollout projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/deliveryPipelines/pop-stats-pipeline/releases/rel-57c07e1/rollouts/rel-57c07e1-to-staging-0001 in target staging
    ...done.
    ```

1.  View the status of the promotion.

    ```bash
    gcloud --project=${PROJECT_ID} deploy releases list --delivery-pipeline=pop-stats-pipeline --region=us-central1 --format='value(targetRenders.staging.renderingState)'
    ```

    The output is similar to the following:

    ```
    SUCCEEDED
    ```

    You can also view this from the GCP Console by navigating to [Cloud Deploy pipelines](https://console.cloud.google.com/deploy/delivery-pipelines).

    You now have the `pop-stats` application deployed to `gke-stage` cluster.

1.  Verify all expected resources are present on `gke-stage` cluster.

    ```bash
    kubectl --context=gke-stage -n default get service,deployment,hpa
    ```

    The output is similar to the following:

    ```
    NAME                 TYPE           CLUSTER-IP      EXTERNAL-IP       PORT(S)          AGE
    service/kubernetes   ClusterIP      10.24.128.1     <none>            443/TCP          43m
    service/pop-stats    LoadBalancer   10.24.129.169   104.154.136.202   8080:30574/TCP   3m55s

    NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/pop-stats   1/1     1            1           3m56s

    NAME                                            REFERENCE              TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
    horizontalpodautoscaler.autoscaling/pop-stats   Deployment/pop-stats   0%/80%    1         5         1          3m57s
    ```

    You can also view this from the GCP Console in the [Kubernetes Engine](https://console.cloud.google.com/kubernetes/) page.

1.  Wait until you see an `EXTERNAL-IP` address populated in the `pop-stats` Service and the Deployment is Available to access the pop-stats application. Navigate to the following address on port 8080:

    ```bash
    kubectl --context=gke-stage -n default get service pop-stats -ojsonpath='{.status.loadBalancer.ingress[].ip}'
    ```

    > Ensure that you are navigating to port 8080 and not the default port 80.

    The top banner on the application should say `Population Stats Staging`. The `Staging` in the banner indicates that it is running on `gke-stage` cluster. You can now promote this pipeline all the way to production.

1.  Navigate to the [Cloud Deploy pipelines](https://console.cloud.google.com/deploy/delivery-pipelines) link.
1.  Click on the pipeline called **pop-stats-pipeline**.
1.  In the **Pipeline visualization** section, click **Promote** in the middle box labeled **staging**.
1.  In the drawer page, you can review the promotion details.
    - Target is set to `prod`.
    - Click on **MANIFEST DIFF** to see the difference between the releases. Since this is your first time deploying to the `prod` environment, the **Proposed release** is all new.
1.  Click **PROMOTE** at the bottom.
1.  In the **Pipeline visualization** screen, between **staging** and **prod** boxes, you should see an orange bubble suggesting that 1 review is pending.
1.  Click on **Review** under the orange bubble.
1.  In the approvals screen, click on **REVIEW**.
1.  The **Approve rollout to prod** looks very similar to the **Promote** screen. Here, you can review the **MANIFEST DIFF** again.
1.  Clicking **SHOW MORE** gives you additional details about the promotion to `prod`.
1.  Once satisfied, click on **APPROVE** at the bottom.
1.  At the top, click on **DELIVERY PIPELINE pop-stats-pipline** link to navigate back to the release page.
1.  You can see the **prod** stage in progress.
1.  Wait until the **prod** stage is complete.

    Your release is now pushed all the way to production.

    You now have the `pop-stats` application deployed to `gke-stage` cluster.

1.  Verify all expected resources are present on `gke-stage` cluster.

    ```bash
    kubectl --context=gke-prod -n default get service,deployment,hpa
    ```

    The output is similar to the following:

    ```
    NAME                 TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
    service/kubernetes   ClusterIP      10.117.0.1     <none>         443/TCP          85m
    service/pop-stats    LoadBalancer   10.117.1.128   34.70.107.49   8080:32121/TCP   8m21s

    NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/pop-stats   10/10   10           10          8m21s

    NAME                                            REFERENCE              TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
    horizontalpodautoscaler.autoscaling/pop-stats   Deployment/pop-stats   0%/50%    1         10        10         8m22s
    ```

    You can also view this from the GCP Console in the [Kubernetes Engine](https://console.cloud.google.com/kubernetes/) page.

1.  Wait until you see an `EXTERNAL-IP` address populated in the `pop-stats` Service and the Deployment is Available. Navigate to the following address on port 8080:

    ```bash
    kubectl --context=gke-prod -n default get service pop-stats -ojsonpath='{.status.loadBalancer.ingress[].ip}'
    ```

    > Ensure that you are navigating to port 8080 and not the default port 80.

    The top banner on the application should say `Population Stats Prod`. The `Prod` in the banner indicates that it is running on `gke-prod` cluster.

### Deploying changes to your application

Now that your application is successfully deployed, you can make changes to your application, test the changes locally in a dev environment and release it through the same workflow as above.

As an application developer, you have access to the `gke-dev` cluster. You can think of the `gke-dev` cluster as a local test environment. You can use any Kubernetes cluster for example minikube. In the application repo, there is a `skaffold.yaml` file which enables the developers to bypass learning complex Kubernetes concepts and simply deploy their applications and changes quickly.

1.  Inspect the `skaffold.yaml` file.

    ```bash
    cat ${WORKDIR}/pop-stats-repo/skaffold.yaml
    ```

    The output is similar to the following:

    ```
    apiVersion: skaffold/v2beta16
    kind: Config
    metadata:
      name: pop-stats
    build:
      ## Uncheck the following 2 lines if you're using minikube locally and do not want to push this to artifact repo
      # local:
      #   push: false
      artifacts:
        - image: pop-stats
          context: app
          docker:
            dockerfile: Dockerfile
    deploy:
      kustomize:
        paths:
          - k8s/overlays/dev
    ```

    The `build` section is responsible for creating the container image and pushing it to Artifact registry. While the `deploy` section deploys the Kubernetes manifests required to run the application. The `path` determines which manifests to deploy. In this example, you deploy the Kubernetes manifests in the `k8s/overlays/dev` folder. The rest of the file is used by Cloud Deploy for the release pipeline.

    Before you can use skaffold, you must config your environment.

1.  Set your kubeconext to use the `gke-dev` cluster. This is the cluster skaffold uses to deploy your dev environment. You can set your kubecontext to any cluster for example a local minikub cluster.

    ```bash
    kubectl config use-context gke-dev
    ```

1.  Configure your default repo to be used for container builds and configure authentication.

    ```bash
    skaffold config set default-repo us-central1-docker.pkg.dev/$PROJECT_ID/pop-stats
    gcloud auth configure-docker us-central1-docker.pkg.dev
    ```

    This repo is used to push/pull container images as you make changes.

1.  Change the search button on the pop-stats app from `btn-secondary` to `btn-primary`.

    ```bash
    sed -i 's/btn-secondary/btn-primary/' ${WORKDIR}/pop-stats-repo/app/templates/base.html
    ```

1.  Build and deploy using skaffold.

    ```bash
    skaffold run --port-forward
    ```

    This will build a new container image and deploy it to the `gke-dev` cluster.

    The output should end with the following:

    ```
    ...
    Waiting for deployments to stabilize...
    - deployment/pop-stats is ready.
    Deployments stabilized in 9.008 seconds
    Port forwarding deployment/pop-stats in namespace , remote port 8080 -> http://127.0.0.1:8080
    Port forwarding service/pop-stats in namespace default, remote port 8080 -> http://127.0.0.1:8081
    Press Ctrl+C to exit
    ```

1.  In the top right corner of Cloud Shell, click on the **Web Preview** icon (square icon with a circle inside) and click on **Preview on port 8080**.
1.  A new tab should open and you can see the frontend with the search button now shows blue (primary button color in Bootstrap).
1.  Navigate to the **Kubernetes Engine** page in GCP Console and inspect the **Workloads**. You can see that there is now a Deployment in the `gke-dev` cluster (which skaffold has deployed for testing).
1.  Back in Cloud Shell, enter **Ctrl+C** to exit skaffold.
1.  Once you are satisfied with your change, run the following command to stop skaffold.

    ```bash
    skaffold delete
    ```

    The output is similar to the following:

    ```
    Cleaning up...
    - service "pop-stats" deleted
    - deployment.apps "pop-stats" deleted
    - horizontalpodautoscaler.autoscaling "pop-stats" deleted
    ```

    Skaffold deletes all the resources it deployed.

### Releasing the change to production

You are now ready to push your change to production.

1.  In order to push the change, commit the change to the `pop-stats-repo`.

    ```bash
    cd ${WORKDIR}/pop-stats-repo
    # Commit to source repo
    git add . && git commit -am "changed search button to btn-primary"
    git push -u origin main
    ```

1.  Inspect the Cloud Build pipeline and wait until the status changes to `SUCCESS`. The output shows two builds. The top build is the latest/current one.

    ```bash
    watch gcloud builds list
    ```

    The output is similar to the following:

    ```
    ID: 1aae6c7e-b24b-49ab-8bbd-2efa497a4309
    CREATE_TIME: 2022-02-09T21:13:40+00:00
    DURATION: 1M47S
    SOURCE: pop-stats-repo@71c5d4ee17114bf4c5f44229d7b1c9fb5c3d1353
    IMAGES: -
    STATUS: SUCCESS # This changes from QUEUED to WORKING to SUCCESS
    ...
    ```

    Press **Ctrl+C** to exit the watch command.

    Once the Cloud Build completes successfully, the delivery pipeline kicks off. You can follow the steps above to promote this release to production. ALternatively, you can run the following commands to promote the change directly to `prod`.

    ```bash
    # Get the release name
    export RELEASE_NAME=$(gcloud --project=${PROJECT_ID} deploy releases list --delivery-pipeline=pop-stats-pipeline --region=us-central1 --format='value(name)' --limit 1)
    # Promote release directly to prod
    gcloud --project=${PROJECT_ID} deploy releases promote --release=${RELEASE_NAME} --delivery-pipeline=pop-stats-pipeline --region=us-central1 --to-target=prod
    # Approve the release to prod
    export RELEASE_ID=$(gcloud --project=${PROJECT_ID} deploy releases list --delivery-pipeline=pop-stats-pipeline --region=us-central1 --format='value(name)' | awk -F "/" 'NR==1 {print $NF}')
    gcloud --project=${PROJECT_ID} deploy rollouts approve ${RELEASE_ID}-to-prod-0001 --release=${RELEASE_ID} --delivery-pipeline=pop-stats-pipeline --region=us-central1
    ```

    Navigate to the [Cloud Deploy pipelines](https://console.cloud.google.com/deploy/delivery-pipelines/us-central1/pop-stats-pipeline) Console page and wait until the `prod` stage finishes successfully.

    Confirm that the `prod` instances of the `pop-stats` app now show a blue search button.

### Rolling back changes

Every once in a while, you may have a bad change make it all the way top production. Cloud Deploy allows you to quickly rollback to any of the previous releases. Assume that you do not want the search button to be `btn-primary` and want to revert back to `btn-secondary` (as it orginially was). You can do this both via the GCP Console UI as well as via Cloud Shell CLI.

1.  Get the delivery pipeline details by running the following command:

    ```bash
    gcloud --project=${PROJECT_ID} deploy delivery-pipelines describe pop-stats-pipeline --region=us-central1
    ```

    The output is similar to the following:

    ```
    ...
    stages:
    - profiles:
        - test
        targetId: test
    - profiles:
        - staging
        targetId: staging
    - profiles:
        - prod
        targetId: prod
    ...
    ```

    There are three targets in this pipeline. You can select any target for rolling back. In this case, lets assume you want to rollback the `prod` target (or the production environment).

1.  Run the following command to rollback the `prod` environment to the previous release.

    ```bash
    export ROLLBACK_RELEASE_ID=$(gcloud --project=${PROJECT_ID} deploy releases list --delivery-pipeline=pop-stats-pipeline --region=us-central1 --format='value(name)' | awk -F "/" 'NR==2 {print $NF}')
    gcloud --project=${PROJECT_ID} deploy targets rollback prod \
      --release=${ROLLBACK_RELEASE_ID} \
      --delivery-pipeline=pop-stats-pipeline \
      --region=us-central1
    ```

    The output is similar to the following:

    ```
    Rolling back target prod to release rel-57c07e1.
    Do you want to continue (Y/n)?
    Creating rollout projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/deliveryPipelines/pop-stats-pipeline/releases/rel-57c07e1/rollouts/rel-57c07e1-to-prod-0002 in target prod...working.
    Creating rollout projects/qwiklabs-gcp-01-20981ec5ef22/locations/us-central1/deliveryPipelines/pop-stats-pipeline/releases/rel-57c07e1/rollouts/rel-57c07e1-to-prod-0002 in target prod...done.
    The rollout is pending approval.
    ```

1.  Approve the rollback by running the following command:

    ```bash
    gcloud --project=${PROJECT_ID} deploy rollouts approve ${ROLLBACK_RELEASE_ID}-to-prod-0002 --release=${ROLLBACK_RELEASE_ID} --delivery-pipeline=pop-stats-pipeline --region=us-central1
    ```

    The output is similar to the following:

    ```
    Approving rollout rel-71c5d4e-to-prod-0002 from rel-71c5d4e to target prod.
    Do you want to continue (Y/n)?
    ```

1.  Navigate to the [Cloud Deploy pipelines](https://console.cloud.google.com/deploy/delivery-pipelines/us-central1/pop-stats-pipeline) Console page and wait until the `prod` stage finishes successfully.

    Verify that the `prod` environment rolled back successfully by navigating to the app and inspecting the search button.

1.  You can now access the application via the pop-stats Service LoadBalancer IP. Navigate to the following address on port 8080:

    ```bash
    kubectl --context=gke-prod -n default get service pop-stats -ojsonpath='{.status.loadBalancer.ingress[].ip}'
    ```

    > Ensure that you are navigating to port 8080 and not the default port 80.

    After the deployments have restarted, the search button should now be gray signifying the rollback has successfully completed.

## Conclusion

In this lab, you learned the following:

- Creating and running an application delivery pipeline using [Cloud Build](https://cloud.google.com/build) and [Cloud Deploy](https://cloud.google.com/deploy).
- Safely promoting an application to production through a multi-stage pipeline (using both GCP Console UI and CLI).
- Developing, testing and deploying changes to your application.
- Releasing changes to production.
- Rolling back changes.
