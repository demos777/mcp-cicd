/**
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "Unique identifer of the Google Cloud Project that is to be used"
  type        = string
}

variable "region" {
  description = "Google Cloud Region in which the GKE clusters are provisioned"
  type        = string
  default = "us-central1"
}

variable "apis" {
  description = "List of Google Cloud APIs to be enabled for this lab"
  type        = list(string)
  default = [
    "container.googleapis.com",
    "cloudbuild.googleapis.com",
    "artifactregistry.googleapis.com",
    "clouddeploy.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "sourcerepo.googleapis.com",
    "logging.googleapis.com",
    "iam.googleapis.com",
    "compute.googleapis.com",
  ]
}

variable "source_repo" {
    description = "Cloud Source Repository name for source code and K8s manifests"
    type        = string
    default = "pop-stats-repo"
}

variable "branch_name" {
    description = "Cloud Source Repository branch name to trigger Cloud Build"
    type        = string
    default = "main"
}

variable "artifact_repo" {
    description = "Artifact Repository name for container images"
    type        = string
    default = "pop-stats"
}

variable "gke_dev" {
    description = "GKE dev cluster name"
    type        = string
    default = "gke-dev"
}

variable "gke_test" {
    description = "GKE test cluster name"
    type        = string
    default = "gke-test"
}

variable "gke_stage" {
    description = "GKE stage cluster name"
    type        = string
    default = "gke-stage"
}

variable "gke_prod" {
    description = "GKE prod cluster name"
    type        = string
    default = "gke-prod"
}