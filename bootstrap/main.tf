/**
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

data "google_project" "project" {}

module "enable_google_apis" {
  source                      = "terraform-google-modules/project-factory/google//modules/project_services"
  version                     = "11.2.3"
  project_id                  = var.project_id
  activate_apis               = var.apis
  disable_services_on_destroy = false
}

resource "google_sourcerepo_repository" "source-repo" {
  name = var.source_repo
  depends_on = [
    module.enable_google_apis
  ]
}

resource "google_cloudbuild_trigger" "push-to-main" {
  trigger_template {
    branch_name = var.branch_name
    repo_name   = google_sourcerepo_repository.source-repo.name
  }
  filename = "cloudbuild.yaml"
}

resource "google_artifact_registry_repository" "artifact-repo" {
  provider = google-beta

  location = var.region
  repository_id = var.artifact_repo
  format = "DOCKER"
  depends_on = [
    module.enable_google_apis
  ]
}

resource "google_container_cluster" "gke-dev" {
  name     = var.gke_dev
  location = var.region 
  enable_autopilot = true
  depends_on = [
    module.enable_google_apis
  ]
}

resource "google_container_cluster" "gke-test" {
  name     = var.gke_test
  location = var.region 
  enable_autopilot = true
  depends_on = [
    module.enable_google_apis
  ]
}

resource "google_container_cluster" "gke-stage" {
  name     = var.gke_stage
  location = var.region 
  enable_autopilot = true
  depends_on = [
    module.enable_google_apis
  ]
}

resource "google_container_cluster" "gke-prod" {
  name     = var.gke_prod
  location = var.region 
  enable_autopilot = true
  depends_on = [
    module.enable_google_apis
  ]
}

resource "google_project_iam_binding" "cloudbuild-iam-clouddeploy-releaser" {
  role    = "roles/clouddeploy.releaser"

  members = [
    "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com",
  ]
}

resource "google_project_iam_binding" "cloudbuild-iam-serviceaccountuser" {
  role    = "roles/iam.serviceAccountUser"

  members = [
    "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com",
  ]
}